FROM archlinux:base-devel

RUN pacman -Syu --noconfirm && \
    pacman -S glfw-x11 vulkan-headers shaderc glm --noconfirm && \
    rm /var/cache/pacman/pkg/* && \
    useradd -m builder

VOLUME ["/src"]

USER builder

WORKDIR /src
